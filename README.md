The Selling Labs offers sales and leadership support for tech leaders through executive coaching and workshops. We optimise business performance by developing and implementing proven sales strategies gleaned from our experience in global tech companies.

Address: 20 Harcourt Street, St Kevin's, Dublin 2 D02 H364, Ireland

Phone: +353 86 827 3284

Website: http://www.thesellinglabs.ie
